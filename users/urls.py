from django.urls import path
from users import views


urlpatterns = [
    path('', views.home, name='home'),
    path('register_user/', views.user_register, name='register_user'), 
    path('view_user/', views.user_view, name='view_user'),
    path('update_user/<int:id>', views.user_update, name='update_user'),
    path('list_user/', views.user_list, name='list_user'),
    path('delete_user/<int:id>', views.user_delete, name='delete_user'),
    path('view_specific_user/<int:id>', views.view_specific_user, name='view_specific_user'),
]
