from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required
from users.models import User
from .forms import RegisterForm, UpdateForm
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

# Create your views here.

@login_required
def home(request):
    """
    You can show the index view in your proyect
    """
    return render(request, 'base2.html')



def user_register(request):
    """
    You can register users in your database and show the register form.
    """

    if request.method == 'POST':
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            register_form.save()
            return redirect('login')
    else:
        register_form = RegisterForm()
    
    context = {
        'register_form': register_form
    }

    return render(request, 'users/user_register.html', context)

@login_required
def user_view(request):
    """
    You can show the user view in your proyect
    """
    return render(request, 'users/user_view.html')

@permission_required('users.change_my_user')
def user_update(request, id):
    """
    You can update the user in your proyect
    """
    update_form = None
    error = None
    status = None
    try:
        user = User.objects.get(id = id)
        if request.method == 'GET':
            update_form = UpdateForm(instance = user)
        else:
            update_form = UpdateForm(request.POST, instance = user)
            if update_form.is_valid():
                update_form.save()
            return redirect('home')
    except ObjectDoesNotExist:
        error = "Client Does not exist"
        
    context = {
        'update_form': update_form,
        'error': error,
        'status': status,
        'u': id
    }
    return render(request, 'users/user_update.html', context)



@permission_required('users.view_all_users')
def user_list(request):
    """
    You can show your clients of your database
    """
    user = User.objects.filter(is_active=True)

    context = {
        'users': user
    }

    return render(request, 'users/user_list.html', context)

@permission_required('users.delete_user')
def user_delete(request, id):
    """
    Delete a client changing his status account to False
    """
    user = User.objects.get(id = id)
    user.is_active = False
    user.save()
    return redirect('list_user')


@permission_required('users.view_user')
def view_specific_user(request, id):
    """
    You can see a client specific data
    """
    user = None
    error = None
    try:
        user = User.objects.get(id = id)
    except ObjectDoesNotExist:
        error = "Client Does not exist"

    context = {
        'item': user,
        'error': error,
    }
    return render(request, 'users/user_specific_view.html', context)