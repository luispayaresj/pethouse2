from django.contrib import admin
from users.models import User, Profile
from django.contrib.auth.models import Permission

# Register your models here.


class ProfileInline(admin.StackedInline):
    model = Profile
    extra = 1

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    inlines = [ProfileInline]
    list_display = ('id','first_name', 'last_name', 'email', 'username', 'is_active')
    search_fields = ('id', 'first_name', 'email', 'username', 'is_active')



@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'is_vet', 'is_client', 'photo')

admin.site.register(Permission)





