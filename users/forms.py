from django.contrib.auth.models import User 
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm


class RegisterForm(UserCreationForm):

    username = forms.CharField(max_length=30, required=True, label="Username", widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter your Username'}))
    first_name = forms.CharField(max_length=30, required=False, label="First Name",
                                 widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter your first name'}))
    last_name = forms.CharField(max_length=30, required=False, label="Last Name",
                                widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter your last name'}))
    email = forms.EmailField(required=False, label="Email",
                             widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Enter your email'}))
    cell_phone = forms.CharField(label="Phone Number", widget=(forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter your Phone'})))
    address = forms.CharField(label="Address", widget=(forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter your Address'})))
    
    password1 = forms.CharField(label="Password", widget=(forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Enter your password'})))
        
    password2 = forms.CharField(label="Confirm Password", widget=(forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Confirm your password'})))
    

    class Meta:
        model = get_user_model()
        fields = ["username", "first_name", "last_name",
                  'email','cell_phone','address','password1', 'password2']


class UpdateForm(forms.ModelForm):
    
        username = forms.CharField(max_length=30, required=True, label="Username", widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter your Username'}))
        first_name = forms.CharField(max_length=30, required=False, label="First Name",
                                 widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter your first name'}))
        last_name = forms.CharField(max_length=30, required=False, label="Last Name",
                                widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter your last name'}))
        email = forms.EmailField(required=False, label="Email",
                             widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Enter your email'}))
        cell_phone = forms.CharField(label="Phone Number", widget=(forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter your Phone'})))
       
        address = forms.CharField(label="Address", widget=(forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter your Address'})))
        class Meta:
            model = get_user_model()
            fields = ["username", "first_name", "last_name",
                        'email', 'cell_phone', 'address']
    