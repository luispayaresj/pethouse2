from django.contrib import admin

# Register your models here.
from pets.models import Pet

@admin.register(Pet)
class PetAdmin(admin.ModelAdmin):
    list_display = ('id','name','breed','species','age','owner','gender')
    search_fields = ('owner','name','breed','gender','species')
    list_filter = ('breed','age','weight','gender','species')
