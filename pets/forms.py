from django import forms
from pets.models import Pet


class PetForm(forms.ModelForm):
    options = (
        ('Years', 'Years'),
        ('Months', 'Months')
    )

    options2 = (
        ('Female', 'Female'),
        ('Male', 'Male')
    )

    options3 = (
        ('Dog','Dog'),
        ('Cat','Cat'),
        ('Rabbit','Rabbit'),
        ('Bird','Bird'),
        ('Pig','Pig'),
        ('Lizard','Lizard'),
        ('Snake','Snake')
    )

    units = forms.CharField(label='Is age in years or months?',
                            widget=forms.Select(choices=options, attrs={'class': 'form-gender'}))
    weight = forms.FloatField(label='Weight must be in kilograms')

    species = forms.CharField(label="Species", widget=forms.Select(choices=options3, attrs={'class': 'form-gender'}))

    gender = forms.CharField(label="Gender", widget=forms.Select(choices=options2, attrs={'class': 'form-gender'}))

    class Meta:
        model = Pet
        fields = ['name', 'age', 'units', 'weight',
                  'breed','species','gender']
