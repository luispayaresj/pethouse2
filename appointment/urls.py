from django.urls import path
from appointment import views


urlpatterns = [
    path('create/<int:id>', views.createAppointment, name='create_appoint'),
    path('list/<int:id>', views.listAppointment, name='list_appoint'),
    path('update/<int:id>', views.updateAppointments, name='update_appoint'),
    path('list_all/', views.listAllAppointment, name='list_all_appoint'),
]
