from django.contrib import admin

from appointment.views import createAppointment
from .models import CreateAppointment

# Register your models here.


@admin.register(CreateAppointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ('pet', 'type_service', 'date')
    list_filter = ['pet', 'type_service']



